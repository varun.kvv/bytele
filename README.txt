-----------------------------
Byte.ly URL Shortener Service
-----------------------------

This service provides a bunch of functionality:

1. Takes a url and returns a byte.ly link.
> curl localhost:8080/api/shorten/google.com
http://byte.ly/4w3oepkyza2n


2. Takes a byte.ly link and returns the full url if available
> curl localhost:8080/api/elongate/http%3A%2F%2Fbyte.ly%2F4w3oepkyza2n
google.com

3. Provides end point that increments count
> curl localhost:8080/api/incr/http%3A%2F%2Fbyte.ly%2F4w3oepkyza2n
1
> curl localhost:8080/api/incr/http%3A%2F%2Fbyte.ly%2F4w3oepkyza2n
2

4. Provides end point that gets count
> curl localhost:8080/api/count/http%3A%2F%2Fbyte.ly%2F4w3oepkyza2n
2


--------------------
Implementation Notes
--------------------

- I did not implement this to be used directly from, say, the browser, where the server would return a redirect.
Built it under the assumption that there has to be a service like this that would (at some point) handle more complicated concerns like custom domains and such.
Hope that's okay.

- I used http4s because that's the "easiest" server I could bolt on. Consequently had to convert my futures to tasks because http4s works best with tasks.
Not a biggie IMO, I think Task > Future anyway.

- Added some comments as to various places where I was being lazy.

- You should be able to access git history, if you're interested ;)