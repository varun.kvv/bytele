package ly.byte

import org.scalatest.FunSuite

class hashTest extends FunSuite {


  test("it should return the correct hash for urls") {
    assert(hash("http://google.com") === "63j234a2x7kf")
    assert(hash("http://www.nydailynews.com/news/memorial-services-slain-emt-yadira-arroyo-gallery-1.3007223") === "8wbdl8416z8h")

    // non ascii chars work
    assert(hash("http://www.nydailynews.com/news/memoriaƒ-services-slain-emt-yadira-arroyo-gallery-1.3007223") === "maatcxiwtook")
  }

  test("conversion of numbers to alphanumeric works correctly") {
    assert(alphaNumeric(1) === "1")
    assert(alphaNumeric(10) === "a")
    assert(alphaNumeric(100) === "s2")
  }


}
