package ly.byte

import org.scalatest.FunSuite

import scala.concurrent.ExecutionContext.Implicits.global

class UrlShortenerTest extends FunSuite {

  test( "basic get / set ops work" ) {
    val url = "https://google.com"
    val urlShortener = new UrlShortener
    val shortened = urlShortener.shorten("https://google.com").run
    assert(urlShortener.elongate(shortened).run === Some(url))
  }

  test( "searching for non existent key returns None" ) {
    val url = "https://google.com"
    val urlShortener = new UrlShortener
    assert(urlShortener.elongate("asdf").run === None)
  }

  test("click counts") {
    val url = "https://google.com"
    val urlShortener = new UrlShortener
    val shortened = urlShortener.shorten("https://google.com").run

    urlShortener.incrCount(shortened).run
    urlShortener.incrCount(shortened).run

    assert(urlShortener.getCount(shortened).run === Some(2))

  }

}
