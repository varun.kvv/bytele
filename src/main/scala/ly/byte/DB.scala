package ly.byte

import scala.concurrent.ExecutionContext
import scalaz.concurrent.Task

class DBOps(implicit val ec: ExecutionContext) {

  // just doing this to emulate that the key value store would return a future
  private class KVStore[K,V](implicit val ec: ExecutionContext) {
    import scala.collection.mutable
    private var internal = mutable.Map[K, V]()
    def get(k: K): Task[Option[V]] = {
      Task(internal.get(k))
    }
    def set(k: K, v: V): Task[Unit] = Task {
      internal = internal + (k -> v)
    }
  }

  // yep, everything deleted when app is restarted
  private val clicksDB = new KVStore[String, Int]()
  private val hashDB = new KVStore[String, String]()

  def set(url: String, hash: String): Task[Unit] = {
    hashDB.set(hash, url)
  }

  def resolveShortened(hash: String): Task[Option[String]] =
    hashDB.get(hash)

  // not optimal, we'd rather use a database that supported atomic increment
  // or use an append only data store (think kafka), and do the aggregation on the read side
  def incr(hash: String): Task[Int] = {
    clicksDB.get(hash).flatMap {
      case Some(i) =>
        clicksDB.set(hash, i + 1)
          .map(_ => i + 1)
      case _ =>
        clicksDB.set(hash, 1)
          .map(_ => 1)
    }
  }

  def getCount(hash: String): Task[Option[Int]] =
    clicksDB.get(hash)

}
