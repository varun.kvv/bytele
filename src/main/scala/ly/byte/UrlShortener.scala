package ly.byte

import scala.concurrent.ExecutionContext
import scalaz.concurrent.Task

class UrlShortener(implicit val ec: ExecutionContext) {

  // let's assume we don't do custom domains
  private val domain = "http://byte.ly"

  // would usually pass this in as an argument to this class (either implicitly or explicitly) for the purpose of testing
  // being a bit lazy here
  private val db = new DBOps

  private def hashFragment(shortenedUrl: String): Option[String] = {
    val substrIndex = shortenedUrl.indexOf(s"$domain/")
    if(substrIndex < 0)
      None
    else
      Some( shortenedUrl.splitAt(substrIndex + domain.length + 1)._2 )

  }

  def shorten(url: String): Task[String] = {
    val hashed = hash(url)
    db.set(url, hashed)
      .map( _ => s"$domain/$hashed" )
  }

  def elongate(shortenedUrl: String): Task[Option[String]] = {
    hashFragment(shortenedUrl)
      .map(db.resolveShortened)
      .getOrElse(Task(Option.empty[String]))
  }

  def getCount(shortenedUrl: String): Task[Option[Int]] = {
    hashFragment(shortenedUrl)
      .map(db.getCount)
      .getOrElse(Task(None))
  }

  def incrCount(shortenedUrl: String): Task[Option[Int]] = {
    hashFragment(shortenedUrl)
      .map(h => db.incr(h).map(Some(_)))
      .getOrElse(Task(None))
  }

}
