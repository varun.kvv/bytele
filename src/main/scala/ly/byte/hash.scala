package ly.byte

object hash {
  // overly simple - ton of other options available, I just picked one :/
  def apply(str: String): String =
    alphaNumeric(Math.abs(str.hashCode)) ++
      alphaNumeric(Math.abs(str.reverse.hashCode))
}

object alphaNumeric {
  private val digits = "0123456789abcdefghijklmnopqrstuvwxyz"

  private def alphaNumericChar(i: Int): Char = {
    if(i < 0 || i > digits.length - 1)
      throw new Exception("overflow: cannot convert")
    else
      digits.charAt(i)
  }

  def apply(num: Int): String = {
    val cardinality = 26 + 10 // alphabets and numbers
    var internalNumber = num
    var s = new StringBuilder
    while(internalNumber != 0) {
      s = s + alphaNumericChar(internalNumber % cardinality)
      internalNumber = internalNumber / cardinality
    }
    s.mkString
  }

}
