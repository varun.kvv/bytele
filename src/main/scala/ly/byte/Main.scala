package ly.byte

import scalaz.concurrent.Task
import org.http4s._
import org.http4s.dsl._
import org.http4s.server.blaze._
import org.http4s.server.{Server, ServerApp}


object Main extends ServerApp {

  import scala.concurrent.ExecutionContext.Implicits.global

  private val urlShortener = new UrlShortener

  val shortenerService = HttpService {
    case request @ GET -> Root / "shorten" / url =>
      Ok(urlShortener.shorten(url))

    case request @ GET -> Root / "elongate" / url =>
      urlShortener.elongate(url).run match {
        case Some(s) =>
          Ok(s)
        case None =>
          NotFound()
      }

    case request @ GET -> Root / "incr" / url =>
      urlShortener.incrCount(url).run match {
        case Some(n) =>
          Ok(n.toString)
        case None =>
          NotFound()
      }

    case request @ GET -> Root / "count" / url =>
      urlShortener.getCount(url).run match {
        case Some(n) =>
          Ok(n.toString)
        case None =>
          NotFound()
      }

    case request @ GET -> Root / "ping" =>
      Ok("pong")
  }

  override def server(args: List[String]): Task[Server] = {
    BlazeBuilder
      .bindHttp(8080, "localhost")
      .mountService(shortenerService, "/api")
      .start
  }
}
